package com.zuitt.batch193;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private String address;

    public User(){}

    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }

    public String getFirstName(){return this.firstName;}
    public String getLastName() {return this.lastName;}
    public int getAge() {return this.age;}
    public String getAddress() {return this.address;}

    public void setFirstName(String firstName) {this.firstName = firstName;}
    public void setLastName(String lastName) {this.lastName = lastName;}
    public void setAge(int age) {this.age = age;}
    public void setAddress(String address) {this.address = address;}
}
