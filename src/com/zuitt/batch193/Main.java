package com.zuitt.batch193;

public class Main {

    public static void main(String[] args) {

        User baker = new User("Rocket", "Racoon", 25, "MCU");
        System.out.println(baker.getFirstName());
        System.out.println(baker.getLastName());
        System.out.println(baker.getAge());
        System.out.println(baker.getAddress());

        Course firstCourse = new Course();
        firstCourse.setName("Baking and Pastry");
        firstCourse.setDescription("A once a week, 6 hour course for culinary students");
        firstCourse.setSeats(25);
        firstCourse.setFee(14000.00);
        firstCourse.setStartDate("July 1, 2022");
        firstCourse.setEndDate("September 30, 2022");

        System.out.println(firstCourse.getName());
        System.out.println(firstCourse.getDescription());
        System.out.println(firstCourse.getSeats());
        System.out.println(firstCourse.getFee());
        System.out.println(firstCourse.getStartDate());
        System.out.println(firstCourse.getEndDate());
        System.out.println(firstCourse.getInstructorFirstName());

        Course secondCourse = new Course("Garde Manger", "An introduction to appetizers and soups", 30, 10000.00, "July 1, 2022", "September 30, 2022");

        System.out.println(secondCourse.getName());
        System.out.println(secondCourse.getDescription());
        System.out.println(secondCourse.getSeats());
        System.out.println(secondCourse.getFee());
        System.out.println(secondCourse.getStartDate());
        System.out.println(secondCourse.getEndDate());

        Course newCourse = new Course();
        System.out.println(newCourse.getInstructorFirstName());
    }
}
